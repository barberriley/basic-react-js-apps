import React from 'react';

var App = React.createClass({
  getInitialState: function(){
    return ({
      clicks: 0
    })
  },
  counter: function(){
    this.setState({
      clicks: this.state.clicks + 1
    })
  },
  render(){
    return(<div>
      <p>Clicks: {this.state.clicks}</p>
      <img
        src="https://lh3.googleusercontent.com/9cy676Ow2f_5cDRSwdgGhYRqcOBq4pNyda1UXI89zyr0Pakr-p9SJaGHPWyMRXrof-sL=w300"
        alt="click me"
        onClick={this.counter}
        />
    </div>
    );
  }
});

export default App;
